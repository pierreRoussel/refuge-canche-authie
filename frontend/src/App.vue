<template>
  <div id="app">
    <header>
      <div id="main-title" class="menu-title top-align centered">
        <h1 class="primary-dark">Les animaux ont un mérite : ils ne déçoivent jamais</h1>
      </div>
      <div class="menu-pin top-align right-align" role="pin accès réseaux sociaux">
        <a
          href="https://www.facebook.com/S.P.A.CancheAuthie/"
          target="_blank"
          rel="noopener noreferrer"
          class="facebook-pin"
          title="Facebook"
        >
          <font-awesome-icon :icon="['fab', 'facebook']" />
        </a>
        <a
          href="https://www.instagram.com/spa_canche_authie/"
          target="_blank"
          rel="noopener noreferrer"
          class="instagram-pin"
          title="Instagram"
          alt="lien page instagram spa canche authie"
        >
          <font-awesome-icon class="instagram-pin" :icon="['fab', 'instagram']" />
        </a>
      </div>

      <div class="menu menu-container primary back navbar">
        <div class="menu-row logo centered">
          <img
            src="./assets/logo.png"
            title="Lien page accueil"
            alt="refuge st-aubin"
            @click="redirect($router)"
          />
        </div>

        <div class="menu-row links centered primary-dark back">
          <router-link to="/" exact class="item">
            <font-awesome-icon class="menu-icon" icon="home" />Accueil
          </router-link>
          <router-link to="/actualite" exact class="item">Actualités</router-link>
          <router-link to="/equipe" exact class="item">L'équipe</router-link>
          <router-link to="/map" exact class="item">Plan & accès</router-link>
          <router-link
            to="/panel"
            exact
            class="item item--admin"
            v-if="this.authenticated"
          >Panel administrateur</router-link>
        </div>
      </div>
    </header>
    <div class="body-container centered">
      <FlashMessage></FlashMessage>
      <router-view />
    </div>

    <div class="footer-container centered">
      <div class="credits">
        Réalisé avec
        <font-awesome-icon id="love" class="menu-icon" icon="heart" />par
        <a href="https://www.linkedin.com/in/pierre-roussel1/">Pierre ROUSSEL</a>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  name: "app",
  created: function() {
    if (localStorage.getItem("jwt") !== null) {
      this.authenticated = true;
    } else {
      this.authenticated = false;
    }
  },
  methods: {
    redirect: function(route) {
      if (route.currentRoute.name !== "home") route.push({ name: "home" });
    }
  }
};
</script>

<style>
#app {
  width: 100%;
  max-width: 100vw;
  margin: 0;
}
.menu-row a.router-link-active,
.menu-row a.router-link-exact-active.item--admin {
  color: white;
  background: var(--success);
}
.menu-row a.item--admin {
  color: var(--success);
}
#love {
  color: var(--alert);
}
#main-title {
  color: black;
  width: 100%;
  position: absolute;
  z-index: 1000;
  font-family: Shadows, Cambria, Cochin, Georgia, Times, "Times New Roman",
    serif;
  font-weight: lighter;
}
#main-title > h1 {
  display: flow-root;
}
.menu > .logo {
  display: inline;
  text-align: left;
  z-index: 10000;
  position: relative;
}
.menu > .logo > img {
  display: inline-block;
  width: auto;
  height: 12vh;
  cursor: pointer;
  border-radius: 50%;
  transition: 0.4s;
}

.menu > .logo > img {
  box-shadow: 4px 4px 5px var(--primary-dark);
}

.menu > .logo > img:hover {
  box-shadow: 1px 1px 15px 5px var(--primary-dark);
}
input {
  width: 300px;
}
.menu-icon {
  padding-right: 0.5em;
}
div.label {
  width: 120px;
}
div.input {
  margin-bottom: 10px;
}
button.ui.button {
  margin-top: 15px;
  display: block;
}

.menu-pin {
  z-index: 1000;
  position: sticky;
  text-align: right;
  padding: 0.5em 0.1em;
  font-size: 0.7em;
  color: var(--primary);
}

.menu-pin i {
  text-align: center;
}

.menu-pin a {
  padding: 0.5em 0.3em;
  font-size: 2.8em;
}

.menu-pin a:last-child {
  padding-right: 1em;
}
</style>