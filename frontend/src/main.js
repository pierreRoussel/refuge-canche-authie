import Vue from 'vue'
import App from './App.vue'

import './assets/theme.css';
import 'leaflet/dist/leaflet.css';

import router from './router'
import {
  library
} from '@fortawesome/fontawesome-svg-core'
import {
  faPeopleCarry,
  faPlusCircle,
  faHome,
  faHandHoldingHeart,
  faHeart,
  faRoute
} from '@fortawesome/free-solid-svg-icons'
import {
  FontAwesomeIcon,
} from '@fortawesome/vue-fontawesome'
import {
  faFacebook,
  faInstagram
} from '@fortawesome/free-brands-svg-icons'

/** Messages flash */
import FlashMessage from '@smartweb/vue-flash-message';
Vue.use(FlashMessage);

/** Import de chaque icone */
library.add(faPeopleCarry, faPlusCircle, faHome, faHandHoldingHeart, faHeart, faFacebook, faInstagram, faRoute);
Vue.component('font-awesome-icon', FontAwesomeIcon);

/** Persist les données avec vue */
import persistentState from 'vue-persistent-state'

const initialState = {
  authenticated: false
}

Vue.use(persistentState, initialState)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')