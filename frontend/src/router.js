import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue'
import Map from './views/Map.vue';
import Actualite from './views/Actualite.vue';
import Login from './views/BO/Login.vue';
import Panel from './views/BO/Panel.vue';

Vue.use(Router);

let router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    linkActiveClass: 'active',
    routes: [{
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/map',
            name: 'map',
            component: Map
        },
        {
            path: '/actualite',
            name: 'actualite',
            component: Actualite
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                guest: true
            }
        },
        {
            path: '/panel',
            name: 'panel',
            component: Panel,
            meta: {
                requiresAuth: true
            }
        }

    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('jwt') === null) {
            next({
                path: '/login',
                params: {
                    nextUrl: to.fullPath
                }
            })
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.guest)) {
        if (localStorage.getItem('jwt') === null) {
            next()
        } else {
            next({
                name: 'panel'
            })
        }
    } else {
        next()
    }
})

export default router