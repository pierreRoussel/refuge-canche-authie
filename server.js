require('dotenv').config();

const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');

global.Task = require('./api/models/taskModel');
global.User = require('./api/models/userModel');
const secureRoutes = require('./api/routes/adminRoutes');
const signRoutes = require('./api/routes/signRoutes');
require('./auth/auth');

/* connexion mongo */
const uri = process.env.MONGO_CONNECTION_URL;
mongoose.connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});
mongoose.connection.on('error', (error) => {
    console.log(error);
    process.exit(1);
});
mongoose.connection.on('connected', function () {
    console.log('Connexion BDD OK');
});
mongoose.Promise = global.Promise;

const port = process.env.PORT || 3000;
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use('/', signRoutes);
app.use('/bo', passport.authenticate('jwt', {
    session: false
}), secureRoutes);
app.listen(port);

app.use((req, res) => {
    res.status(404).send({
        url: `${req.originalUrl} not found`
    });
});

console.log(`Accès local : http://localhost:3000`);