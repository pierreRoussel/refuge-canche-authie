const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const UserModel = require('../api/models/userModel');

//middleware passport pour l'enregistration d'un nouvel utilisateur
passport.use('signup', new localStrategy({
    usernameField: 'identifiant',
    passwordField: 'password'
}, async (identifiant, password, done) => {
    try {
        const user = await UserModel.create({
            identifiant,
            password
        });
        return done(null, user);
    } catch (error) {
        return done(error);
    }
}));

//middleware passport pour la connexion d'un utilisateur
passport.use('login', new localStrategy({
    usernameField: 'identifiant',
    passwordField: 'password'
}, async (identifiant, password, done) => {
    try {
        const user = await UserModel.findOne({
            identifiant
        });
        if (!user) {
            return done(null, false, {
                message: 'Utilisateur introuvable'
            });
        }
        const validate = await user.isValidPassword(password);
        if (!validate) {
            return done(null, false, {
                message: 'Mauvais mot de passe'
            });
        }
        return done(null, user, {
            message: 'Connexion réussie'
        });
    } catch (error) {
        return done(error);
    }
}));

passport.use(new JWTstrategy({
    secretOrKey: 'top_secret',
    jwtFromRequest: ExtractJWT.fromUrlQueryParameter('secret_token')
}, async (token, done) => {
    try {
        return done(null, token.user);
    } catch (error) {
        done(error);
    }
}));