const mongoose = require('mongoose');
const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');

exports.create_a_user = async (req, res) => {
    passport.authenticate('signup', {
        session: false
    }, async (req, res) => {
        res.json({
            message: 'Compte créé.',
            user: req.user
        });
    })
};

exports.user_log_in = (req, res, next) => {
    res.json({
        message: 'hello'
    })
    passport.authenticate('login', async (err, user) => {
        try {
            if (err || !user) {
                const error = new Error('Erreur de connexion')
                return next(error);
            }
            req.login(user, {
                session: true
            }, async (error) => {
                if (error) return next(error)
                const body = {
                    _id: user._id,
                    email: user.email
                };
                const token = jwt.sign({
                    user: body
                }, 'top_secret');
                return res.json({
                    token
                });
            });
        } catch (error) {
            return next(error);
        }
    })(req, res, next);
}

exports.liste_all_employe = (req, res, next) => {
    res.json({
        message: 'You made it to the secure route',
        user: req.user,
        token: req.query.secret_token
    })
}