var express = require('express');
const passport = require('passport');
var router = express.Router();
const jwt = require('jsonwebtoken');

router.post('/signup', passport.authenticate('signup', {
    session: false
}), async (req, res, next) => {
    res.json({
        message: 'Inscription réussie',
    });
});

router.post('/signin', async (req, res, next) => {
    passport.authenticate('login', async (err, user, info) => {
        try {
            if (err || !user) {
                throw ('Echec de la connexion. ' + info.message);
            }
            req.login(user, {
                session: false
            }, async (error) => {
                if (error) return next(error)
                const body = {
                    _id: user._id,
                    email: user.email
                };
                const token = jwt.sign({
                    user: body
                }, 'top_secret', {
                    expiresIn: '30m'
                });
                return res.json({
                    user: user,
                    token: token
                });
            });
        } catch (error) {
            return res.json({
                error: error
            });
        }
    })(req, res, next);
});

module.exports = router;