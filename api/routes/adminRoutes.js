const userBuilder = require('../controllers/userController');

module.exports = app => {
    app
        .route('/bo/employe')
        .get(userBuilder.liste_all_employe)
};