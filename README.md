# Projet webapp - Refuge Canche-Authie

Utilisation : MVN stack

## Back-end API

dev: 
```
npm i
npm start
```


## Front-end Vuejs

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
